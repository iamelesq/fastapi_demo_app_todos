from fastapi import FastAPI, Depends
from database import engine
from starlette.staticfiles import StaticFiles
from starlette import status
from starlette.responses import RedirectResponse

import models
from routers import auth
from routers import todos

app = FastAPI()

models.Base.metadata.create_all(bind=engine)

# import static directory to enable adding
# the static files to the app.
app.mount("/static", StaticFiles(directory="static"), name="static")


# handle redirects from the root of the app to the /todos homepage
@app.get("/")
async def root():
    return RedirectResponse(url="/todos", status_code=status.HTTP_302_FOUND)


app.include_router(auth.router)
app.include_router(todos.router)

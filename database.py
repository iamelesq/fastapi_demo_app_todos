from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


SQLALCHEMY_DATABASE_URL = "mysql+pymysql://root:@localhost:3306/todoapp"

# settings related to sqlite
engine = create_engine(
    # sqlite version
    # SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    # mysql
    SQLALCHEMY_DATABASE_URL
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# create the database model we will inherit in future
Base = declarative_base()

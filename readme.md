# FastAPI / Jinja2 example app

Typical and almost universal `todos` sample app for the purpose of trying out fastAPI. Basic UI was knocked-up with Jinja templating and the app uses a local development mysql DB. Not worth deploying to a cloud instance as the app offers the basic todo app functionality with crud options, with completion strikethrough capabilities and some jwt authentocation. All standard features for a typical produtivity/todos app.
